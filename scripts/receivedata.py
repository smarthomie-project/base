#!/usr/bin/env python

import socket
import os

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
s.bind(('', 32157))

while True:
	try:
		message, address = s.recvfrom(64)
		print "Received %s from " % message, address
		s.sendto('smarthomie-base', address)
	except (KeyboardInterrupt, SystemExit):
		print "closed."
		s.close()
		break
