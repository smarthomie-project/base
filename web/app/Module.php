<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Module extends Model
{
    public function temperatureValues () {
		return $this->hasMany(TemperatureValue::class);
	}

	public function lastTemperatureValue () {
		if (!$this->hasMany(TemperatureValue::class)) {
			return "n/a";
		}
		return $this->hasMany(TemperatureValue::class)->orderBy('created_at', 'desc')->first();
	}

	public function temperatureValuesMinusHours ($hours) {
		$temperatures = TemperatureValue::where('module_id', $this->id)->where('created_at', '>', Carbon::now()->subHours($hours))->where('created_at', '<', Carbon::now()->subHours($hours-1));
		$sum = $temperatures->sum('value');
		$average = $temperatures->count() == 0 ? 1 : $temperatures->count();

		return number_format($sum / $average, 2);
	}

	public function devices () {
		return $this->hasMany(RemoteSocketDevice::class);
	}
}
