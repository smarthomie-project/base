<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class TemperatureValue extends Model
{
    public function module () {
		return $this->belongsTo(Module::class);
	}
}
