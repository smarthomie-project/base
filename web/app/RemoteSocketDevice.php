<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemoteSocketDevice extends Model
{
	public function module () {
		return $this->belongsTo(Module::class);
	}
}
