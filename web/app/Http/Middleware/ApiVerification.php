<?php

namespace App\Http\Middleware;

use Closure;

use Carbon\Carbon;

use App\Module;
use App\Config;

class ApiVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (!$request->header('php-auth-user')) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Please specify php-auth-user in the header.',
			]);
		}

		if (!$request->header('php-auth-pw')) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Please specify php-auth-pw in the header.',
			]);
		}

		$macAddress = $request->header('php-auth-user');
		$apiKey = $request->header('php-auth-pw');

		if (!Module::where('mac_address', $macAddress)->first()) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Invalid MAC address.',
			]);
		}

		$module = Module::where('mac_address', $macAddress)->first();

		if ($module->api_key != $apiKey) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Invalid API key.',
			]);
		}

		// if ($module->ip_address != $request->ip()) {
		// 	return response()->json([
		// 		'status' => 'error',
		// 		'status_message' => 'The IP address does not fit with the one in the database. Please remove the module and add it again.',
		// 	]);
		// }

		$module->last_seen = Carbon::now();
		$module->save();

        return $next($request);
    }
}
