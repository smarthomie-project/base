<?php

namespace App\Http\Middleware;

use Closure;

use App\Config;

class MasterApiVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (!$request->header('php-auth-user') || $request->header('php-auth-user') != 'master_api_key') {
			return response()->json([
				'status' => 'error',
				'status_message' => 'php-auth-user is invalid.',
			]);
		}

		if (!$request->header('php-auth-pw')) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Please specify php-auth-pw in the header.',
			]);
		}

		$apiKey = $request->header('php-auth-pw');

		if (!Config::where('key', 'master_api_key')->exists()) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Master API key is not set.',
			]);
		}

		$masterApiKey = Config::where('key', 'master_api_key')->first()->value;

		if ($apiKey != $masterApiKey) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Key is invalid.',
			]);
		}

        return $next($request);
    }
}
