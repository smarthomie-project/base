<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Guzzle;

use App\Config as ModelConfig;

class PushBulletController extends Controller
{
	private $apiKey;

	public function __construct() {
		$this->apiKey = ModelConfig::where('key', 'pushbullet_api_key')->first()->value;
	}

    public function listPushes($active = 'true') {
		$response = Guzzle::request(
			'GET',
			'https://api.pushbullet.com/v2/pushes?active=' . $active,
			[
				'verify' => false,
				'headers' => [
					'Access-Token' => $this->apiKey,
				],
			]
		);

		return $response;
	}

	public function createPush($body, $title = 'SmartHomie', $receiver_email = null) {
		$response = Guzzle::request(
			'POST',
			'https://api.pushbullet.com/v2/pushes',
			[
				'verify' => false,
				'headers' => [
					'Access-Token' => $this->apiKey,
					'Content-Type' => 'application/json',
				],
				'json' => [
					'type' => 'note',
					'title' => $title,
					'body' => $body,
					'email' => $receiver_email,
				]
			]
		);

		return $response;
	}

	public function deletePush($iden) {
		$response = Guzzle::request(
			'DELETE',
			'https://api.pushbullet.com/v2/pushes/' . $iden,
			[
				'verify' => false,
				'headers' => [
					'Access-Token' => $this->apiKey,
				],
			]
		);

		return $response;
	}

	public function deleteAllPushes() {
		$response = Guzzle::request(
			'DELETE',
			'https://api.pushbullet.com/v2/pushes',
			[
				'verify' => false,
				'headers' => [
					'Access-Token' => $this->apiKey,
				],
			]
		);

		return $response;
	}
}
