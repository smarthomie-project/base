<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use SSH;
use Config;

use App\Module;
use App\TemperatureValue;

class ApiController extends Controller
{
	public function registerModule (Request $request) {
		$ipAddress = $request->ip();
		$macAddress = $request->mac_address;
		$type = $request->type;

		if (Module::where('mac_address', $macAddress)->count()) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'This module is already registered.',
			]);
		}

		if (strlen($macAddress) != 12) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Invalid MAC Address provided.',
			]);
		}

		if (!$type) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Please provide the module type.',
			]);
		}

		$random = str_random(32);

		Config::set('remote.connections.' . $random . '.host', $ipAddress . ':32156');
		Config::set('remote.connections.' . $random . '.username', 'pi');
		Config::set('remote.connections.' . $random . '.key', env('SSH_KEY_FILE'));
		Config::set('remote.connections.' . $random . '.keyphrase', '');

		SSH::into($random)->run([
			'echo "smarthomie-base-connect"',
		], function($line) {
			if ($line != "smarthomie-base-connect") {
				return response()->json([
					'status' => 'error',
					'status_message' => 'Could not connect to module\'s management interface.',
				]);
			}
		});

		$module = new Module;
		$module->ip_address = $ipAddress;
		$module->mac_address = $macAddress;
		$module->last_seen = Carbon::now();
		$module->api_key = str_random(64);
		$module->type = $type;
		$module->save();

		return response()->json([
			'status' => 'success',
			'status_message' => 'API key has been created.',
			'api_key' => $module->api_key,
		]);
	}

	public function updateIPAddress(Request $request) {
		$macAddress = $request->header('php-auth-user');
		$module = Module::where('mac_address', $macAddress)->first();

		$ipAddress = $request->ip_address;

		if ($ipAddress != $module->ip_address) {
			$module->ip_address = $ipAddress;
			$module->save();

			return response()->json([
				'status' => 'success',
				'status_message' => 'IP address successfully updated.',
			]);
		}

		return response()->json([
			'status' => 'error',
			'status_message' => 'IP address is the same.',
		]);
	}

	public function updateTemperature (Request $request) {
		$macAddress = $request->header('php-auth-user');
		$module = Module::where('mac_address', $macAddress)->first();

		if (!$request->degree_celcius) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Please specify degree_celcius.',
			]);
		}

		$temperatureValue = new TemperatureValue;
		$temperatureValue->module_id = $module->id;
		$temperatureValue->value = $request->degree_celcius;
		$temperatureValue->save();

		return response()->json([
			'status' => 'success',
			'status_message' => 'Temperature has been updated.',
		]);
	}

	public function deleteModule (Request $request) {
		if (!$request->module_id) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Please specify module_id.',
			]);
		}

		if (!Module::find($request->module_id)) {
			return response()->json([
				'status' => 'error',
				'status_message' => 'Module does not exist.',
			]);
		}

		$module = Module::find($request->module_id);
		Module::find($request->module_id)->delete();

		return response()->json([
			'status' => 'success',
			'status_message' => 'Module ' . $module->mac_address . ' has been deleted.',
		]);
	}
}
