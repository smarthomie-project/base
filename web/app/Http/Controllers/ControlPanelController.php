<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Module;
use App\RemoteSocketDevice;
use App\Config as ModelConfig;
use App\WakeOnLanDevice;

use Config;
use SSH;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use View;

class ControlPanelController extends Controller
{
    public function getIndex () {
		return view('index', [
			'modules' => Module::all(),
			'configs' => ModelConfig::all(),
			'wake_on_lan_devices' => WakeOnLanDevice::all(),
			'update_available' => md5(trim(file_get_contents(env('VERSION_FILE')))) == md5(trim(file_get_contents('https://gitlab.com/smarthomie-project/base/raw/master/conf/version') ? null : file_get_contents('https://gitlab.com/smarthomie-project/base/raw/master/conf/version'))),
		]);
	}

	public function postIndex(Request $request) {
		if (isset($request->config)) {
			foreach ($request->config as $key => $value) {
				$configline = ModelConfig::where('key', $key)->first();
				$configline->value = $value;
				$configline->save();
			}
		}

		if (isset($request->edit)) {
			foreach ($request->edit as $id => $edit) {
				$module = Module::find($id);
				$module->location = $edit['location'];
				$module->description = $edit['description'];
				$module->save();
			}
		}

		if (isset($request->delete)) {
			foreach ($request->delete as $delete => $id) {
				Module::find($id)->delete();
			}
		}

		if (isset($request->device)) {
			foreach ($request->device as $id => $edit) {
				if ($id == "new") {
					if (!isset( $edit['description'], $edit['device_id'], $edit['frequency'])) {
						break;
					}

					$device = new RemoteSocketDevice;
					$device->module_id = $request->module_id;
					$device->description = $edit['description'];
					$device->device_id = $edit['device_id'];
					$device->frequency = $edit['frequency'];
					$device->save();
				} else {
					$device = RemoteSocketDevice::find($id);
					$device->description = $edit['description'];
					$device->device_id = $edit['device_id'];
					$device->frequency = $edit['frequency'];
					$device->save();
				}
			}
		}

		if (isset($request->delete_device)) {
			foreach ($request->delete_device as $delete => $id) {
				RemoteSocketDevice::find($id)->delete();
			}
		}

		if (isset($request->wol)) {
			foreach ($request->wol as $id => $edit) {
				if ($id == "new") {
					if (!isset( $edit['description'], $edit['mac_address'])) {
						break;
					}

					$device = new WakeOnLanDevice;
					$device->description = $edit['description'];
					$device->mac_address = $edit['mac_address'];
					$device->save();
				} else {
					$device = WakeOnLanDevice::find($id);
					$device->description = $edit['description'];
					$device->mac_address = $edit['mac_address'];
					$device->save();
				}
			}
		}

		if (isset($request->delete_wol)) {
			foreach ($request->delete_wol as $delete => $id) {
				WakeOnLanDevice::find($id)->delete();
			}
		}

		return redirect(route('cp.index'));
	}

	public function anyWakeOnLan(Request $request, $mac_address) {
		$process = new Process('sudo etherwake ' . $mac_address);
		$process->run();

		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}

		if (isset($request->dynamic)) {
			return response()->json([
				'status' => 'success',
				'output' => $process->getOutput(),
			]);
		}

		return redirect(route('cp.index'));
	}

	public function anyRemoteSocketChangeState (Request $request, $device_id, $action) {
		$device = RemoteSocketDevice::find($device_id);

		$socketsCharacters = ['A', 'B', 'C', 'D', 'E', 'F'];
		$socketID = array_search($device->device_id, $socketsCharacters) + 1;

		switch ($action) {
			case 'on':
				$action = 1;
				$device->state = 1;
				break;

			case 'off':
				$action = 0;
				$device->state = 0;
				break;
		}

		$random = str_random(32);

		Config::set('remote.connections.' . $random . '.host', $device->module->ip_address . ':32156');
		Config::set('remote.connections.' . $random . '.username', 'pi');
		Config::set('remote.connections.' . $random . '.key', env('SSH_KEY_FILE'));
		Config::set('remote.connections.' . $random . '.keyphrase', '');

		SSH::into($random)->run([
			'cd /srv/smarthomie/scripts/',
			'./main.sh ' . $device->frequency . ' ' . $socketID . ' ' . $action,
		], function ($response) {
			//
		});

		$device->save();

		if (isset($request->dynamic)) {
			return response()->json([
				'status' => 'success',
			]);
		}

		return redirect(route('cp.index'));
	}

	public function anyLoadModule (Request $request, $view = null) {
		if (!isset($view)) {
			return abort(404);
		}

		if (!View::exists('index.' . $view)) {
			return abort(404);
		}

		return view('index.' . $view, [
			'modules' => Module::all(),
			'configs' => ModelConfig::all(),
			'wake_on_lan_devices' => WakeOnLanDevice::all(),
		]);
	}
}
