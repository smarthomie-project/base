<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Carbon\Carbon;

use App\Module;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
		/**
		 * Check whether modules are offline or not and notify user if negative respone
		 */
		$schedule->call(function () {
			$modules = Module::where('type', 'temperature');

			foreach ($modules as $module) {
				if ($module->last_seen < Carbon::now()->subMinutes(30)) {
					$response = file_get_contents(route('pushbullet.create-push',
						[
							$module->description . ' (' . $module->ip_address . ') has been offline for more than 30mins now!',
						]
					));
				}
			}
        })->everyMinute();

		/**
		 * Check for new commands
		 */
		$schedule->call(function() {
			commandProcessor();
		});
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
