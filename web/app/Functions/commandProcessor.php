<?php

use App\Module;

function commandProcessor () {
	$response = file_get_contents(route('pushbullet.list-pushes'));

	$data = json_decode($response);

	foreach ($data->pushes as $push) {
		if ($push->active && $push->body) {
			$iden = $push->iden;
			$body = trim($push->body);
			$receiver_email = $push->receiver_email;

			if (substr($body, 0, 1) == ':') {
				$filtered = commandParser($body);

				switch ($filtered->command) {
					case 'temp':
					case 'temperature':
						if (synopsisError($filtered->arguments, 1, 'temp <location> [other locations...]')) break;

						foreach ($filtered->arguments as $location) {
							$modules = Module::where('type', 'temperature')->where('location', $location);

							if (!$modules->count()) {
								file_get_contents(route('pushbullet.create-push',
									[
										'No temperature modules registered at "' . $location . '".'
									]
								));
								break;
							}

							$modules = $modules->get();

							foreach ($modules as $module) {
								file_get_contents(route('pushbullet.create-push',
									[
										'Temperature at ' . $module->location . ' (' . $module->description . ') is ' . $module->lastTemperatureValue()->value . ' °C.'
									]
								));
							}
						}
						break;
					case 'remote':
					case 'socket':
					case 'remotesocket':
					case 'turn':
						if (synopsisError($filtered->arguments, 3, 'remotesocket <location> <device description> <on|off>')) break;

						$location = $filtered->arguments[0];
						$deviceDescription = $filtered->arguments[1];
						$action = $filtered->arguments[2];

						$modules = Module::where('type', 'remote-socket')->where('location', $location);

						if (!$modules->count()) {
							file_get_contents(route('pushbullet.create-push',
								[
									'No remote socket modules available at "' . $location . '".',
								]
							));
							break;
						}

						switch ($action) {
							case 'on':
							case 'off':
								break;
							default:
								file_get_contents(route('pushbullet.create-push',
									[
										'Action is incorrect! Correct on|off',
									]
								));
								break;
						}

						$deviceFound = false;

						foreach ($modules->get() as $module) {
							$devices = $module->devices;

							if ($devices->count()) {
								foreach ($devices as $device) {
									echo $device->description;
									if (trim($device->description) == $deviceDescription) {
										$deviceFound = true;

										file_get_contents(route('cp.remote-socket.change-state',
											[
												$device->id,
												$action,
											]
										));

										file_get_contents(route('pushbullet.create-push',
											[
												'Turned ' . $action . '" ' . $deviceDescription . '" at "' . $location . '".',
											]
										));
									}
								}
							}
						}

						if (!$deviceFound) {
							file_get_contents(route('pushbullet.create-push',
								[
									'No devices found called "' . $deviceDescription . '" at "' . $location . '".',
								]
							));
						}

						break;
					case 'help':
						file_get_contents(route('pushbullet.create-push',
							[
								'[ :temperature | :temp ], [ :remotesocket | :remote | :socket ]',
								'Available commands'
							]
						));
						break;
					default:
						file_get_contents(route('pushbullet.create-push',
							[
								'Unknown command "' . $filtered->command . '". Type ":help" for a list of commands.',
							]
						));
						break;
				}

				file_get_contents(route('pushbullet.delete-push',
					[
						$iden,
					]
				));
			}
		}
	}
}
