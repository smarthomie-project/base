<?php

function commandParser ($input, $indicator = ':') {
	$output = new stdClass();

	$input = trim($input);

	$exploded = explode(' ', str_replace($indicator, '', $input));
	$output->command = $exploded[0];
	unset($exploded[0]);
	$output->arguments = array_values($exploded);

	foreach ($output->arguments as $index => $argument) {
		$firstChar = substr($argument, 0, 1);
		$lastChar = substr($argument, -1);

		if ($firstChar == '"' || $firstChar == '\'') {
			$argument = str_replace('"', '', $argument);
			$argument = str_replace('\'', '', $argument);

			$done = false;
			$current = $index;
			$fullCommand = $argument;
			$unset = [];

			if ($lastChar == '"' || $lastChar == '\'') {
				$done = true;
			}

			while (!$done) {
				$current++;
				$argument = $output->arguments[$current];

				$lastChar = substr($argument, -1);

				if ($lastChar == '"' || $lastChar == '\'') {
					$argument = str_replace('"', '', $argument);
					$argument = str_replace('\'', '', $argument);
					$done = true;
				}

				unset($output->arguments[$current]);
				$fullCommand .= ' ' . $argument;
			}

			$output->arguments[$index] = $fullCommand;
		}

		if (empty($argument)) {
			unset($output->arguments[$index]);
		}
	}

	$output->arguments = array_values($output->arguments);

	return $output;
}

function synopsisError ($arguments, $min, $synopsis, $indicator = ':') {
	if (count($arguments) < $min) {
		file_get_contents(route('pushbullet.create-push', [$indicator . $synopsis, 'Synopsis Error']));
		return true;
	}

	return false;
}
