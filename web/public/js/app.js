/*******************************
 *	Set date and time in header
 *******************************/

function updateDateTime() {
	var date = new Date();

	var day = ('0' + date.getDate()).slice(-2);
	var month = ('0' + date.getMonth()).slice(-2); + 1;
	var year = date.getFullYear();

	var hour = ('0' + date.getHours()).slice(-2);
	var minutes = ('0' + date.getMinutes()).slice(-2);
	var seconds = ('0' + date.getSeconds()).slice(-2);

	var newdate = day + '.' + month + '.' + year;
	var newtime = hour + ':' + minutes + ':' + seconds;

	$('#current_time').html(newtime);
	$('#current_date').html(newdate);
}

setInterval(updateDateTime);

/*******************************
 *	Modals
 *******************************/

$('.open-modal').click(function (e) {
	e.preventDefault();

	$('#' + $(this).data('id')).fadeIn();
});

$('.modal').click(function(e) {
	if (e.target == this) {
		$('.modal').fadeOut();
	}
});

$(document).on('keyup', function(e) {
	if (e.keyCode == 27) {
		$('.modal').fadeOut();
	}
});
