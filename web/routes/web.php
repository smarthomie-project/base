<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return redirect(route('cp.index'));
});

Route::prefix('cp')->group(function () {
	Route::name('cp.index')->get('', 'ControlPanelController@getIndex');
	Route::post('', 'ControlPanelController@postIndex');

	Route::name('cp.remote-socket.change-state')->any('remotesocket/{device_id}/{action}', 'ControlPanelController@anyRemoteSocketChangeState');

	Route::name('cp.wol')->any('wol/{mac_address}', 'ControlPanelController@anyWakeOnLan');

	Route::name('cp.loadmodule')->any('loadmodule/{view?}', 'ControlPanelController@anyLoadModule');
});

Route::prefix('pushbullet')->group(function () {
	Route::name('pushbullet.list-pushes')->get('list-pushes', 'PushBulletController@listPushes');
	Route::name('pushbullet.create-push')->get('create-push/{body}/{title?}/{receiver_email?}', 'PushBulletController@createPush');
	Route::name('pushbullet.delete-push')->get('delete-push/{iden}', 'PushBulletController@deletePush');
	Route::name('pushbullet.delete-all-pushes')->get('delete-all-pushes', 'PushBulletController@deleteAllPushes');
});

Route::prefix('api')->group(function () {
	Route::get('', function () {
	    abort(404);
	});

	Route::post('register-module', 'ApiController@registerModule');

	Route::middleware('api-verification')->group(function () {
		Route::post('update-temperature', 'ApiController@updateTemperature');
		Route::post('update-ip-address', 'ApiController@updateIPAddress');
	});

	Route::middleware('master-api-verification')->group(function () {
		Route::post('delete-module', 'ApiController@deleteModule');
	});

	Route::get('base-status', function () {
		return response()->json([
			'status' => 'success',
			'status_message' => 'This is a smarthomie base.',
		]);
	});
});
