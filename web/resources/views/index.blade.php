<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link href="{{ asset('/css/app.css') }}" rel="stylesheet" />
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i" rel="stylesheet" />

		<title>SmartHomie Control Panel</title>

		<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
		<script src="{{ asset('/js/Chart.bundle.min.js') }}"></script>

		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/images/favicon/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/images/favicon/favicon-32x32.png') }}">
		<link rel="icon" type="image/png" sizes="194x194" href="{{ asset('/images/favicon/favicon-194x194.png') }}">
		<link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/images/favicon/android-chrome-192x192.png') }}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/images/favicon/favicon-16x16.png') }}">
		<link rel="manifest" href="{{ asset('/images/favicon/manifest.json') }}">
		<link rel="mask-icon" href="{{ asset('/images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
		<link rel="shortcut icon" href="{{ asset('/images/favicon/favicon.ico') }}">
		<meta name="msapplication-config" content="{{ asset('/images/favicon/browserconfig.xml') }}">
		<meta name="theme-color" content="#ffffff">
	</head>
	<body>
		<div class="wrapper">
			<div class="main">
				<div class="seperator"></div>

				<header>
					<div class="row">
						<div class="col-3">
							<h1>
								<img src="{{ asset('/images/logo.png') }}" alt="Logo" class="inline-image">
								Control Panel
							</h1>
						</div>

						<div class="col-3 right">
							<span id="current_time"></span><br />
							<span id="current_date"></span>
						</div>
					</div>
				</header>

				<div class="seperator"></div>

				<div class="main-content" id="main-content">
					@include('index.modals')
					@include('index.modules')
					@include('index.control-modules-row')
					@include('index.temperature-graph')
					{{-- @include('index.wol') --}}
					@include('index.settings')
				</div>
			</div>

			<footer class="main-footer">
				Copyright {{ date('Y') }} by <a href="http://floriantraun.at/" target="_blank">Florian Abensperg-Traun</a> &mdash;
				Version {{ file_get_contents(env('VERSION_FILE')) }} &mdash;
				<a href="https://gitlab.com/smarthomie-project" target="_blank">SmartHomie on Gitlab</a>
			</footer>
		</div>

		<script src="{{ asset('/js/app.js') }}"></script>

		<script>
			/*******************************
			 *	Dynamic remote sockets switching
			 *******************************/

			$(document).ready(function () {
				$('.dynamic-anchor').click(function (e) {
					e.preventDefault();
					var element = $(this);
					var loadingID = 'loading-' + element.data('id');
					var url = element.attr('href');

					console.log('Switching socket - ' + url);

					$('#' + loadingID).show();
					element.hide();

					$.post(url, { dynamic: true, _token: "{{ csrf_token() }}" }, function (data) {
						$('#' + loadingID).hide();

						switch (element.data('type')) {
							case 'on':
								element.next().show();
								break;
							case 'off':
								element.prev().show();
								break;
							default:
								element.show();
						}

						console.log('Socket switched - ' + url);
					});
				});

				setInterval(function () {
					console.log(Date() + ": Loading modules");

					$('.loadmodule').each(function () {
						var element = $(this)
						$.get('{{ route('cp.loadmodule') }}/' + element.data('view'), function(data) {
							$(element).replaceWith(data);
						});
					});
				}, 30000);
			});
		</script>
	</body>
</html>
