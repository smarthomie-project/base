<section class="loadmodule" data-view="control-modules-row">
	<div class="row">
		<div class="col-3">
			<h2>
				Remote power/bulb sockets
			</h2>

			<div class="table-wrapper">
				<table>
					<thead>
						<tr>
							<td>
								Location
							</td>

							<td>
								Device Description
							</td>

							<td>
							</td>
						</tr>
					</thead>

					@foreach ($modules->where('type', 'remote-socket') as $module)
						@foreach ($module->devices as $device)
							<tr>
								<td>
									{{ $module->description }}
								</td>

								<td>
									{{ $device->description }}
									({{ $device->frequency }} / {{ $device->device_id }})
								</td>

								<td style="text-align: right;">
									<img src="{{ asset('/images/off.png') }}" class="loading inline-image" id="loading-remoteswitch-{{ $device->id }}-off" />

									<a class="dynamic-anchor" href="{{ route('cp.remote-socket.change-state', [$device->id, 'off']) }}" data-type="on" data-id="remoteswitch-{{ $device->id }}-off" @if (!$device->state)
										style="display: none;"
									@endif>
										<img src="{{ asset('/images/on.png') }}" class="inline-image" alt="">
									</a>

									<a class="dynamic-anchor" href="{{ route('cp.remote-socket.change-state', [$device->id, 'on']) }}" data-type="off" data-id="remoteswitch-{{ $device->id }}-on" @if ($device->state)
										style="display: none;"
									@endif>
										<img src="{{ asset('/images/off.png') }}" class="inline-image" alt="">
									</a>

									<img src="{{ asset('/images/on.png') }}" class="loading inline-image" id="loading-remoteswitch-{{ $device->id }}-on" />
								</td>
							</tr>
						@endforeach
					@endforeach
				</table>
			</div>
		</div>

		<div class="col-3">
			<h2>
				Current temperatures
			</h2>

			<div class="table-wrapper">
				<table>
					<thead>
						<tr>
							<td>
								Location
							</td>

							<td>
								Description
							</td>

							<td style="text-align: right;">
								24h &Oslash;
							</td>

							<td style="text-align: right;">
								Current
							</td>
						</tr>
					</thead>

					@foreach ($modules->where('type', 'temperature') as $module)
						<tr>
							<td>
								{{ $module->location }}
							</td>

							<td>
								{{ $module->description }}
							</td>

							<td style="text-align: right;">
								@if (!$module->temperatureValues->count())
									n/a
								@else
									{{ number_format(
										$module->temperatureValues->where('created_at', '>', \Carbon\Carbon::now()->subHours(24))->sum('value') /
										($module->temperatureValues->where('created_at', '>', \Carbon\Carbon::now()->subHours(24))->count() == 0 ? 1 :
											$module->temperatureValues->where('created_at', '>', \Carbon\Carbon::now()->subHours(24))->count()),
										2
									) }}
								@endif
								°C
							</td>

							<td style="text-align: right;">
								@if (!$module->lastTemperatureValue())
									n/a
								@else
									{{ number_format($module->lastTemperatureValue()->value, 2) }}
								@endif
								°C
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</section>
