<section class="loadmodule" data-view="modules">
	<div class="row">
		<div class="col-6">
			<h2>Modules</h2>

			<form action="" method="post">
				<div class="table-wrapper">
					<table>
						<thead>
							<tr>
								<td>
									Location
								</td>

								<td>
									Description
								</td>

								<td>
									Type
								</td>

								<td>
									IP &mdash; MAC Address
								</td>

								<td>
									Last seen
								</td>

								<td>
									&times;
								</td>
							</tr>
						</thead>

						@foreach ($modules as $module)
							<tr class="@if ($module->last_seen < \Carbon\Carbon::now()->subMinutes(30) && $module->type == "temperature")
								error
							@endif">
								<td>
									<input type="text" name="edit[{{ $module->id }}][location]" value="{{ $module->location }}">
								</td>

								<td>
									<input type="text" name="edit[{{ $module->id }}][description]" value="{{ $module->description }}">
								</td>

								<td style="text-transform: capitalize;">
									@if ($module->type == "remote-socket")
										<a href="#" class="open-modal" data-id="modal-{{ $module->id }}">
											{{ $module->type }}
										</a>
									@else
										{{ $module->type }}
									@endif
								</td>

								<td>
									{{ $module->ip_address }} &mdash;
									{{ $module->mac_address }}
								</td>

								<td class="last_seen">
									{{ $module->last_seen }}
								</td>

								<td>
									<input type="checkbox" name="delete[]" value="{{ $module->id }}">
								</td>
							</tr>
						@endforeach
					</table>
				</div>

				<p>
					<input type="submit" name="" value="Save">
				</p>

				{{ csrf_field() }}
			</form>
		</div>
	</div>
</section>
