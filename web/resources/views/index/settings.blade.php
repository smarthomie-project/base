<div class="row">
	<form action="" method="post">
		<div class="col-6">
			<h2>
				Miscellaneous
			</h2>

			<div class="row">
				<div class="col-3">
					<h3>
						Settings
					</h3>

					<p>
						<label>
							Pushbullet API Key:
							<input type="password" name="config[pushbullet_api_key]" value="{{ $configs->where('key', 'pushbullet_api_key')->first()->value }}">
						</label>
					</p>

					<p>
						<input type="submit" name="" value="Save">
					</p>
				</div>

				<div class="col-3">
					<h3>
						Update
					</h3>

					@if ($update_available)
						There is a new version ( {{ $update_available }} ) available.
						<a href="https://gitlab.com/smarthomie-project/base">https://gitlab.com/smarthomie-project/base</a>
					@else
						You have the most recent Version.
					@endif
				</div>
			</div>
		</div>

		{{ csrf_field() }}
	</form>
</div>
