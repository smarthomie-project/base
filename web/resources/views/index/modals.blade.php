<section class="loadmodule" data-view="modals">
	@foreach ($modules->where('type', 'remote-socket') as $module)
		<div class="modal" id="modal-{{ $module->id }}">
			<div class="modal-content">
				<form action="" method="post">
					<input type="hidden" name="module_id" value="{{ $module->id }}">

					<div class="table-wrapper">
						<table>
							<thead>
								<tr>
									<td>
										Description
									</td>

									<td>
										Frequency
									</td>

									<td>
										Device
									</td>

									<td>
										&times;
									</td>
								</tr>
							</thead>

							@foreach ($module->devices as $device)
								<tr>
									<td>
										<input type="text" name="device[{{ $device->id }}][description]" value="{{ $device->description }}">
									</td>

									<td>
										<input type="text" name="device[{{ $device->id }}][frequency]" value="{{ $device->frequency }}">
									</td>

									<td>
										<select name="device[{{ $device->id }}][device_id]">
											<option value="A" {{ $device->device_id == 'A' ? 'selected' : ''}}>A</option>
											<option value="B" {{ $device->device_id == 'B' ? 'selected' : ''}}>B</option>
											<option value="C" {{ $device->device_id == 'C' ? 'selected' : ''}}>C</option>
											<option value="D" {{ $device->device_id == 'D' ? 'selected' : ''}}>D</option>
											<option value="E" {{ $device->device_id == 'E' ? 'selected' : ''}}>E</option>
											<option value="F" {{ $device->device_id == 'F' ? 'selected' : ''}}>F</option>
										</select>
									</td>

									<td>
										<input type="checkbox" name="delete_device[]" value="{{ $device->id }}">
									</td>
								</tr>
							@endforeach

							<tfoot>
								<tr>
									<td>
										<input type="text" name="device[new][description]" placeholder="New description">
									</td>

									<td>
										<input type="text" name="device[new][frequency]" placeholder="New frequency">
									</td>

									<td>
										<select name="device[new][device_id]">
											<option value="">--SELECT--</option>
											<option value="A">A</option>
											<option value="B">B</option>
											<option value="C">C</option>
											<option value="D">D</option>
											<option value="E">E</option>
											<option value="F">F</option>
										</select>
									</td>

									<td>
										<input type="submit" name="" value="Save &amp; Add">
									</td>
								</tr>
							</tfoot>
						</table>
					</div>

					{{ csrf_field() }}
				</form>
			</div>
		</div>
	@endforeach
</section>
