<section class="loadmodule" data-view="temperature-graph">
	<div class="row">
		<div class="col-6">
			<h2>
				Temperature history
			</h2>

			<canvas id="temperature-chart" height="100"></canvas>

			<script>
				var ctx = $('#temperature-chart');

				new Chart(ctx, {
						type: 'line',
						data: {
							labels: ["last 4-5 hours", "last 3-4 hours", "last 2-3 hours", "last 1-2 hours", "last hour"],
							datasets: [
								@foreach ($modules->where('type', 'temperature') as $module)
									{
										label: '{{ $module->description }}',
										data: [
											{{ $module->temperatureValuesMinusHours(5) }},
											{{ $module->temperatureValuesMinusHours(4) }},
											{{ $module->temperatureValuesMinusHours(3) }},
											{{ $module->temperatureValuesMinusHours(2) }},
											{{ $module->temperatureValuesMinusHours(1) }},
										],
										backgroundColor: [
											'rgba(0, 0, 0, 0)',
										],
										borderColor: [
											'rgba(15, 117, 189, 1)',
										],
										borderWidth: 3
									},
								@endforeach
							]
						},
						options: {
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero:true
									}
								}]
							}
						}
						});
			</script>
		</div>
	</div>
</section>
