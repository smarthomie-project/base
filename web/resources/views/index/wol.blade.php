<div class="row">
	<div class="col-6">
		<h2>
			Wake on LAN
		</h2>

		<form action="" method="post">
			<div class="table-wrapper">
				<table>
					<thead>
						<tr>
							<td>
								Device description
							</td>

							<td>
								MAC address
							</td>

							<td>
								Action
							</td>

							<td>
								&times;
							</td>
						</tr>
					</thead>

					@foreach ($wake_on_lan_devices as $device)
						<tr>
							<td>
								<input type="text" name="wol[{{ $device->id }}][description]" value="{{ $device->description }}">
							</td>

							<td>
								<input type="text" name="wol[{{ $device->id }}][mac_address]" value="{{ $device->mac_address }}" placeholder="Format: 11:22:33:44:55">
							</td>

							<td>
								<img src="{{ asset('/images/01-progress.gif') }}" class="loading inline-image" id="loading-mac-{{ $device->id }}" />

								<a class="dynamic-anchor" href="{{ route('cp.wol', [$device->mac_address]) }}" data-id="mac-{{ $device->id }}">
									<img src="{{ asset('/images/wake.png') }}" alt="Wake up" class="inline-image">
								</a>
							</td>

							<td>
								<input type="checkbox" name="delete_wol[]" value="{{ $device->id }}">
							</td>
						</tr>
					@endforeach

					<tfoot>
						<tr>
							<td>
								<input type="text" name="wol[new][description]" value="">
							</td>

							<td>
								<input type="text" name="wol[new][mac_address]" value="" placeholder="Format: 11:22:33:44:55">
							</td>

							<td colspan="2">
								<input type="submit" name="" value="Save / Add">
							</td>
						</tr>
					</tfoot>
				</table>
			</div>

			{{ csrf_field() }}
		</form>
	</div>
</div>
