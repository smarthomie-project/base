@foreach ($modules->where('type', 'remote-socket') as $module)
	<div class="modal" id="modal-{{ $module->id }}">
		<div class="modal-content">
			<form action="" method="post">
				<input type="hidden" name="module_id" value="{{ $module->id }}">

				<table>
					<thead>
						<tr>
							<td>
								Description
							</td>

							<td>
								Frequency
							</td>

							<td>
								Device
							</td>

							<td>
								&times;
							</td>
						</tr>
					</thead>

					@foreach ($module->devices as $device)
						<tr>
							<td>
								<input type="text" name="device[{{ $device->id }}][description]" value="{{ $device->description }}">
							</td>

							<td>
								<input type="text" name="device[{{ $device->id }}][frequency]" value="{{ $device->frequency }}">
							</td>

							<td>
								<select name="device[{{ $device->id }}][device_id]">
									<option value="A" {{ $device->device_id == 'A' ? 'selected' : ''}}>A</option>
									<option value="B" {{ $device->device_id == 'B' ? 'selected' : ''}}>B</option>
									<option value="C" {{ $device->device_id == 'C' ? 'selected' : ''}}>C</option>
									<option value="D" {{ $device->device_id == 'D' ? 'selected' : ''}}>D</option>
									<option value="E" {{ $device->device_id == 'E' ? 'selected' : ''}}>E</option>
									<option value="F" {{ $device->device_id == 'F' ? 'selected' : ''}}>F</option>
								</select>
							</td>

							<td>
								<input type="checkbox" name="delete_device[]" value="{{ $device->id }}">
							</td>
						</tr>
					@endforeach

					<tfoot>
						<tr>
							<td>
								<input type="text" name="device[new][description]" placeholder="New description">
							</td>

							<td>
								<input type="text" name="device[new][frequency]" placeholder="New frequency">
							</td>

							<td>
								<select name="device[new][device_id]">
									<option value="">--SELECT--</option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="C">C</option>
									<option value="D">D</option>
									<option value="E">E</option>
									<option value="F">F</option>
								</select>
							</td>

							<td>
								<input type="submit" name="" value="Save &amp; Add">
							</td>
						</tr>
					</tfoot>
				</table>

				{{ csrf_field() }}
			</form>
		</div>
	</div>
@endforeach

<div class="row">
	<div class="col-6">
		<h2>Modules</h2>

		<form action="" method="post">
			<table>
				<thead>
					<tr>
						<td>
							Location
						</td>

						<td>
							Description
						</td>

						<td>
							Type
						</td>

						<td>
							IP &mdash; MAC Address
						</td>

						<td>
							Last seen
						</td>

						<td>
							&times;
						</td>
					</tr>
				</thead>

				@foreach ($modules as $module)
					<tr class="@if ($module->last_seen < \Carbon\Carbon::now()->subMinutes(30) && $module->type == "temperature")
						error
					@endif">
						<td>
							<input type="text" name="edit[{{ $module->id }}][location]" value="{{ $module->location }}">
						</td>

						<td>
							<input type="text" name="edit[{{ $module->id }}][description]" value="{{ $module->description }}">
						</td>

						<td style="text-transform: capitalize;">
							@if ($module->type == "remote-socket")
								<a href="#" class="open-modal" data-id="modal-{{ $module->id }}">
									{{ $module->type }}
								</a>
							@else
								{{ $module->type }}
							@endif
						</td>

						<td>
							{{ $module->ip_address }} &mdash;
							{{ $module->mac_address }}
						</td>

						<td class="last_seen">
							{{ $module->last_seen }}
						</td>

						<td>
							<input type="checkbox" name="delete[]" value="{{ $module->id }}">
						</td>
					</tr>
				@endforeach
			</table>

			<p>
				<input type="submit" name="" value="Save">
			</p>

			{{ csrf_field() }}
		</form>
	</div>
</div>

<div class="row">
	<div class="col-3">
		<h2>
			Remote power/bulb sockets
		</h2>

		<table>
			@foreach ($modules->where('type', 'remote-socket') as $module)
				@foreach ($module->devices as $device)
					<tr>
						<td>
							{{ $module->description }}
						</td>

						<td>
							{{ $device->description }}
							({{ $device->device_id }} / {{ $device->frequency }})
						</td>

						<td style="text-align: right;">
							<img src="{{ asset('/images/01-progress.gif') }}" class="loading" id="loading-{{ $device->id }}-on" />

							<a class="remote-socket-switch" href="{{ route('cp.remote-socket.change-state', [$device->id, 'on']) }}" data-id="{{ $device->id }}-on">
								On
							</a>

							&mdash;

							<a class="remote-socket-switch" href="{{ route('cp.remote-socket.change-state', [$device->id, 'off']) }}" data-id="{{ $device->id }}-off">
								Off
							</a>

							<img src="{{ asset('/images/01-progress.gif') }}" class="loading" id="loading-{{ $device->id }}-off" />
						</td>
					</tr>
				@endforeach
			@endforeach
		</table>
	</div>

	<div class="col-3">
		<h2>
			Current temperatures
		</h2>

		<table>
			@foreach ($modules->where('type', 'temperature') as $module)
				<tr>
					<td>
						{{ $module->description }}
					</td>

					<td style="text-align: right;">
						&Oslash;
						@if ($module->tempereValues)
							n/a
						@else
							{{ number_format(
								$module->temperatureValues->where('created_at', '>', \Carbon\Carbon::now()->subHours(24))->sum('value') /
								($module->temperatureValues->where('created_at', '>', \Carbon\Carbon::now()->subHours(24))->count() == 0 ? 1 :
									$module->temperatureValues->where('created_at', '>', \Carbon\Carbon::now()->subHours(24))->count()),
								2
							) }}
						@endif
						°C
					</td>

					<td style="text-align: right;">
						C
						@if (!$module->temperatureValues->count())
							n/a
						@else
							{{ number_format($module->lastTemperatureValue()->value, 2) }}
						@endif
						°C
					</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>

<div class="row">
	<div class="col-6">
		<h3>
			Temperature history
		</h3>

		<canvas id="temperature-chart" height="100"></canvas>

		<script>
			var ctx = $('#temperature-chart');

			new Chart(ctx, {
					type: 'line',
					data: {
						labels: ["last 4-5 hours", "last 3-4 hours", "last 2-3 hours", "last 1-2 hours", "last hour"],
						datasets: [
							@foreach ($modules->where('type', 'temperature') as $module)
								{
									label: '{{ $module->description }}',
									data: [
										{{ $module->temperatureValuesMinusHours(5) }},
										{{ $module->temperatureValuesMinusHours(4) }},
										{{ $module->temperatureValuesMinusHours(3) }},
										{{ $module->temperatureValuesMinusHours(2) }},
										{{ $module->temperatureValuesMinusHours(1) }},
									],
									backgroundColor: [
										'rgba(0, 0, 0, 0)',
									],
									borderColor: [
										'rgba({{ rand(0, 255) }}, {{ rand(0, 255) }}, {{ rand(0, 255) }}, 1)',
									],
									borderWidth: 3
								},
							@endforeach
						]
					},
					options: {
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero:true
								}
							}]
						}
					}
					});
		</script>
	</div>
</div>

<div class="row">
	<div class="col-6">
		<h3>
			Settings
		</h3>

		<form action="" method="post">
			<p>
				<label>
					Pushbullet API Key:
					<input type="password" name="config[pushbullet_api_key]" value="{{ $configs->where('key', 'pushbullet_api_key')->first()->value }}">
				</label>
			</p>

			<p>
				<input type="submit" name="" value="Save">
			</p>
			{{ csrf_field() }}
		</form>
	</div>
</div>
