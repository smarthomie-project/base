# Base

## Introduction

This is the base for SmartHomie. Install this first before anything else.

The install script (base/install/install.sh) installs all requirements and configuration files.

## Requirements

* Any Debian based distribution (tested with Raspbian Jessie Lite)
* Internet connection

## Installation

Install as follows:

```
sudo apt-get update
sudo apt-get install git-core

git clone https://gitlab.com/technikerprojekt/base.git

cd base/install/
./install.sh
```
