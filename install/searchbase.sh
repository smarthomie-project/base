#!/usr/bin/env bash

export LC_ALL=C
CURRENT_DIR=$(pwd)

BASE=$(python $CURRENT_DIR/sendbroadcast.py)
if [ $? -ne 0 ]; then
	exit 1
fi

exit 0
