-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for smarthomie
DROP DATABASE IF EXISTS `smarthomie`;
CREATE DATABASE IF NOT EXISTS `smarthomie` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smarthomie`;

-- Dumping structure for table smarthomie.configs
DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `key` varchar(64) NOT NULL,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table smarthomie.configs: 2 rows
/*!40000 ALTER TABLE `configs` DISABLE KEYS */;
REPLACE INTO `configs` (`id`, `created_at`, `updated_at`, `key`, `value`) VALUES
	(1, NULL, NULL, 'master_api_key', NULL),
	(2, NULL, NULL, 'pushbullet_api_key', NULL);
/*!40000 ALTER TABLE `configs` ENABLE KEYS */;

-- Dumping structure for table smarthomie.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `ip_address` varchar(16) NOT NULL,
  `mac_address` varchar(12) NOT NULL,
  `last_seen` datetime DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `api_key` varchar(64) NOT NULL,
  `type` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smarthomie.modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- Dumping structure for table smarthomie.remote_socket_devices
DROP TABLE IF EXISTS `remote_socket_devices`;
CREATE TABLE IF NOT EXISTS `remote_socket_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `module_id` int(11) NOT NULL,
  `frequency` int(5) NOT NULL,
  `device_id` varchar(1) NOT NULL DEFAULT 'A',
  `description` varchar(50) DEFAULT NULL,
  `state` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `FK__remote_socket_devices__modules` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smarthomie.remote_socket_devices: ~0 rows (approximately)
/*!40000 ALTER TABLE `remote_socket_devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `remote_socket_devices` ENABLE KEYS */;

-- Dumping structure for table smarthomie.temperature_values
DROP TABLE IF EXISTS `temperature_values`;
CREATE TABLE IF NOT EXISTS `temperature_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `module_id` int(11) NOT NULL,
  `value` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `FK__temperature_values__modules` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smarthomie.temperature_values: ~0 rows (approximately)
/*!40000 ALTER TABLE `temperature_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `temperature_values` ENABLE KEYS */;

-- Dumping structure for table smarthomie.wake_on_lan_devices
DROP TABLE IF EXISTS `wake_on_lan_devices`;
CREATE TABLE IF NOT EXISTS `wake_on_lan_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table smarthomie.wake_on_lan_devices: 0 rows
/*!40000 ALTER TABLE `wake_on_lan_devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `wake_on_lan_devices` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
