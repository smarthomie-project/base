#!/usr/bin/env bash

export LC_ALL=C
CURRENT_DIR=$(pwd)

################################################
# run firstchecks.sh
################################################

$CURRENT_DIR/firstchecks.sh $0
if [ $? -ne 0 ]; then
	exit 1
fi

################################################
# Install dependencies
################################################

apt-get update
apt-get --yes upgrade --force-yes
apt-get --yes install expect openssh-client python-pip ntp --force-yes

################################################
# Scan if any other base exists
################################################

$CURRENT_DIR/searchbase.sh
if [ $? -eq 0 ]; then
	echo "===================================================="
	echo "I have found another base on this network. Aborting."
	echo "===================================================="
	exit 1
fi

################################################
# Generate passwords and keys
################################################

MYSQL_ROOT_USER='root'
MYSQL_ROOT_PASSWORD=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
MYSQL_USER='smarthomie'
MYSQL_USER_PASSWORD=$(date +%m | md5sum | base64 | head -c 32 ; echo)
MASTER_API_KEY=$(date +%h | sha256sum | base64 | head -c 64 ; echo)

################################################
# Install other dependencies and configure
# 	MySQL server
################################################

export DEBIAN_FRONTEND="noninteractive"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASSWORD"

apt-get --yes install mysql-server etherwake python3.4 apache2 php5-common php5-mysql libapache2-mod-php5 mysql-client php5-mcrypt php5-gd php5-cli php5-curl curl openssh-server --force-yes

SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"$MYSQL_ROOT_PASSWORD\r\"
expect \"Change the root password?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")

echo $SECURE_MYSQL

apt-get --yes purge expect --force-yes
apt-get --yes autoremove --force-yes

################################################
# MySQL create user, database and migrate
################################################

mysql -u$MYSQL_ROOT_USER -p$MYSQL_ROOT_PASSWORD <<MYSQL_SCRIPT
CREATE DATABASE $MYSQL_USER;
CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASSWORD';
GRANT ALL PRIVILEGES ON $MYSQL_USER.* TO '$MYSQL_USER'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT

mysql -u$MYSQL_USER -p$MYSQL_USER_PASSWORD < $CURRENT_DIR/conf/db_migration_and_seed.sql

mysql -u$MYSQL_USER -p$MYSQL_USER_PASSWORD <<MYSQL_SCRIPT
UPDATE \`smarthomie\`.\`configs\` SET \`value\`="$MASTER_API_KEY" WHERE \`key\`='master_api_key';
MYSQL_SCRIPT

################################################
# Copy SSH keys, change port and start SSH
################################################

mkdir /home/pi/.ssh
cat $CURRENT_DIR/../ssh/smarthomie.key >> /home/pi/.ssh/smarthomie.key
cat $CURRENT_DIR/conf/authorized_keys >> /home/pi/.ssh/authorized_keys
chmod 0600 /home/pi/.ssh/smarthomie.key
killall ssh-agent
eval `ssh-agent`
ssh-add /home/pi/.ssh/smarthomie.key
chown -R pi:pi /home/pi/.ssh/

cat /etc/ssh/sshd_config | sed 's/Port [0-9]*/Port 32156/' > /tmp/smarthomie
mv /tmp/smarthomie /etc/ssh/sshd_config

update-rc.d ssh enable
invoke-rc.d ssh start

################################################
# Move smarthomie files to /srv/smarthomie
################################################

if [ "$CURRENT_DIR" != "/srv/smarthomie/install" ]; then
	mkdir -p /srv/smarthomie/
	cp -r $CURRENT_DIR/../* /srv/smarthomie/
fi

################################################
# copy config files
################################################

# Apache
ln -s /srv/smarthomie/conf/apache2/smarthomie.conf /etc/apache2/sites-available/
(cd /etc/apache2/sites-available/; a2dissite *)
a2ensite smarthomie
a2enmod rewrite
service apache2 restart

# Cronjob
cp $CURRENT_DIR/conf/crontab /etc/cron.d/smarthomie

# copy init file and activate it
cp $CURRENT_DIR/../scripts/daemon.sh /etc/init.d/smarthomie.sh
sudo chmod 755 /etc/init.d/smarthomie.sh
sudo chown root:root /etc/init.d/smarthomie.sh
update-rc.d smarthomie.sh defaults
update-rc.d smarthomie.sh enable

# copy .env file for CP and API
cp /srv/smarthomie/web/.env.example /srv/smarthomie/web/.env

# change database values in .env
cat /srv/smarthomie/web/.env | \
sed "s/^DB_CONNECTION=.*$/DB_CONNECTION=mysql/" | \
sed "s/^DB_HOST=.*$/DB_HOST=127.0.0.1/" | \
sed "s/^DB_PORT=.*$/DB_PORT=3306/" | \
sed "s/^DB_DATABASE=.*$/DB_DATABASE=$MYSQL_USER/" | \
sed "s/^DB_USERNAME=.*$/DB_USERNAME=$MYSQL_USER/" | \
sed "s/^DB_PASSWORD=.*$/DB_PASSWORD=$MYSQL_USER_PASSWORD/" > \
/tmp/smarthomie
mv /tmp/smarthomie /srv/smarthomie/web/.env

# install composer
(cd /srv/smarthomie/scripts/; \
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
php composer-setup.php; \
php -r "unlink('composer-setup.php');";)

# install dependencies, generate application key and change file and folder permissions
(cd /srv/smarthomie/web/; /srv/smarthomie/scripts/composer.phar update)
php /srv/smarthomie/web/artisan key:generate
chmod -R 755 /srv/smarthomie/web/
chmod -R 777 /srv/smarthomie/web/storage/

# change hostname
echo "smarthomiebase" > /etc/hostname
cat /etc/hosts | sed 's/^127\.0\.1\.1.*[a-zA-Z0-9]*$/127.0.1.1       smarthomiebase/g' > /tmp/smarthomie
mv /tmp/smarthomie /etc/hosts

################################################
# Finish and clean up installation
################################################

echo "
MySQL root user: $MYSQL_ROOT_USER
MySQL root password: $MYSQL_ROOT_PASSWORD

MySQL smarthomie user: $MYSQL_USER
MySQL smarthomie password: $MYSQL_USER_PASSWORD

SmartHomie master API key: $MASTER_API_KEY
" > /srv/smarthomie/credentials.txt

service smarthomie start

# request same address
IP_ADDRESS=$(ifconfig | grep inet | head -n1 | sed 's/:/ /g' | awk '{ print $3 }')
echo "send dhcp-requested-address $IP_ADDRESS;" >> /etc/dhcp/dhclient.conf

echo
echo "Installation finished. The system will reboot in 10 seconds."
echo

sleep 10
reboot
