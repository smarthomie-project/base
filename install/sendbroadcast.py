#!/usr/bin/env python

import socket
import subprocess

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

s.settimeout(3)

s.sendto('smarthomie-module', ('255.255.255.255', 32157))
message, address = s.recvfrom(64)

if (message == "smarthomie-base"):
        print address[0]
        sys.exit(0)

sys.exit(1)
